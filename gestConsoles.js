/*!
fichier: gestConsoles-0.1.js
version:0.1
auteur:pascal TOLEDO
date: 2012.12.06
modif:2013.08.08
source: http://legral.fr/intersites/lib/perso/js/
depend de:
	* rien
description:
* reprend l'idée de fenetre de console evaluable de gestLib sans l'associé a une librairie et ecrit le html a la creation (pas d'injection DOM).
* mode d'insertion:document.write
*/
if(typeof(gestLib)==='object')gestLib.loadLib({nom:'gestConsoles',ver:0.1,description:'gestion de consoles'});

/* ************************************************************
***************************************************************/

/* ************************************************************
//
***************************************************************/
function gestionConsole_Menu(menu)
	{
	if(!menu||!menu.titre)return null;
	this.titre=menu.titre;
	this.isVisible=(menu.isVisible)?menu.isVisible:0;
	this.onclick=(menu.onclick)?menu.onclick:'';
	this.classe=(menu.classe)?menu.classe:'lgs_console_Menu';
	return this;
	}
gestionConsole_Menu.prototype.createDiv=function(){return'\n<span id="'+this.titre+'" class="'+this.classe+'" onclick="'+this.onclick+'">'+this.titre+'</span>\n';}


/* ************************************************************
//
***************************************************************/
function gestionConsole_Console(console)
	{
	if(!console||!console.nom)return null;
	
	this.nom=console.nom;
	this.prefixe='console_'+this.nom+'_';
	this.genre=(console.genre)?console.genre:'div';	//div|textarea
	this.classe=(console.classe)?console.classe:'lgs_console_Contenu';

	//initialisation des menus
	this.menus=Array();
	this.menuAdd({titre:'console:'+this.nom,isVisible:1,onclick:'gestConsoles.switchShowHide(\''+this.nom+'\');'});
	this.menuAdd({titre:'Effacer',isVisible:1,onclick:'gestConsoles.clear(\''+this.nom+'\');'});
	return this;
	}
gestionConsole_Console.prototype=
	{
	menuAdd:function(menu)
		{
		if(!menu||!menu.titre)return null;
		if(this.menus[menu.titre])delete this.menus[menu.titre];
		this.menus[menu.titre]=new gestionConsole_Menu(menu);
		}
	,menuAdd_showhide:function()	{this.menuAdd({titre:'console:'+this.nom,isVisible:1,onclick:'gestConsoles.switchShowHide(\''+this.nom+'\');'})}
	,menuAdd_clear:function()	{this.menuAdd({titre:'Effacer',isVisible:1,onclick:'gestConsoles.clear(\''+this.nom+'\');'})}
	,menuAdd_evaluer:function()	{this.menuAdd({titre:'Evaluer',isVisible:1,onclick:'gestConsoles.evaluer(\''+this.nom+'\');'})}
	
	,hide:function(){this.IdCSS_contenu.style.display="none";}
	,show:function(){this.IdCSS_contenu.style.display="block";}
	,switchShowHide:function(){this.IdCSS_contenu.style.display=(this.IdCSS_contenu.style.display=='none')?'block':'none';}
	

	,getValue:function()
		{switch(this.genre)
			{
			case'textarea':return this.IdCSS_contenu.value;break
			case'div':case'span':return this.IdCSS_contenu.innerHTML;break;
			}
		}
	,setValue:function(val)
		{switch(this.genre)
			{
			case'textarea':this.IdCSS_contenu.value=val;break
			case'div':case'span':this.IdCSS_contenu.innerHTML=val;break;
			}
		}

	,clear:function(){this.setValue('');}
	,write:function(txt)
		{
		switch(this.genre)
			{
			case'textarea':this.setValue(this.getValue()+txt+'\n');break;
			case'div':case'span':this.setValue(this.getValue()+txt+'<br>');break;
			}
		
		
		}
	,evaluer:function(){eval(this.getValue());}

	,createDiv:function()
		{
		var out='';
		out+='<div id="'+this.prefixe+'support" draggable="true">'
		out+='<div id="'+this.prefixe+'menu">';
		for(var m in this.menus){out+='\n'+this.menus[m].createDiv();}
		out+='</div>';
		out+='\n<'+this.genre+' id="'+this.prefixe+'contenu" class="'+this.classe+'"></'+this.genre+'>';
		out+='</div>';
//		return out;
		document.write( out );

		//this.IdCSS_support=	document.getElementById(this.prefixe+'support');
		//this.IdCSS_titre=		document.getElementById(this.prefixe+'titre');
		this.IdCSS_contenu=	document.getElementById(this.prefixe+'contenu');
		return null;
		}
	}
/* ************************************************************
// class de gestion des consoles
***************************************************************/
function gestionConsoles()
	{
	this.consoles=Array();
	this.consoleAdd=function(console)
		{
		if(!console||!console.nom)return null;
		if(this.consoles[console.nom])delete this.consoles[console.nom];
		this.consoles[console.nom]=new gestionConsole_Console(console);
		}
	this.menuAdd=function(consoleTitre,menu)
		{
		if(consoleTitre && menu)
				this.consoles[consoleTitre].menuAdd(menu);
		}
	this.hide=function(consoleTitre){if(this.consoles[consoleTitre]){this.consoles[consoleTitre].hide();}}
	this.show=function(consoleTitre){if(this.consoles[consoleTitre]){this.consoles[consoleTitre].show();}}
	this.switchShowHide=function(consoleTitre){if(this.consoles[consoleTitre]){this.consoles[consoleTitre].switchShowHide();}}
	this.clear=function(consoleTitre){if(this.consoles[consoleTitre]){this.consoles[consoleTitre].clear();}}
	this.write=function(consoleTitre,txt){if(this.consoles[consoleTitre]){this.consoles[consoleTitre].write(txt);}}
	this.evaluer=function(consoleTitre){if(this.consoles[consoleTitre]){this.consoles[consoleTitre].evaluer();}}
	this.createDiv=function(consoleTitre){if(this.consoles[consoleTitre]){return this.consoles[consoleTitre].createDiv();}}
	}

/* ************************************************************
// instance de gestion des consoles
***************************************************************/
gestConsoles=new gestionConsoles();
if(typeof(gestLib)==='object')gestLib.end('gestConsoles');
